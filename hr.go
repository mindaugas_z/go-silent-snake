package ss

import (
	"github.com/boltdb/bolt"
)

// create lib for this
func NewMR(db *bolt.DB, namespace string, version string) *MR {
	mr := new(MR)
	mr.db = db
	mr.namespace = []byte(namespace + "masters" + version)
	mr.db.Update(func(tx *bolt.Tx) error {
		tx.CreateBucketIfNotExists(mr.namespace)
		return nil
	})
	return mr
}

type MR struct {
	names     []string
	cur       int
	db        *bolt.DB
	namespace []byte
	def       string
}

func (mr *MR) SetDefault(s string) {
	mr.def = s
}

func (mr *MR) Len() int {
	return len(mr.names)
}

func (mr *MR) Next() {
	mr.cur++
	if mr.cur > len(mr.names)-1 {
		mr.cur = 0
	}
}

func (mr *MR) Current() string {
	if len(mr.names) == 0 {
		return mr.def
	}
	return mr.names[mr.cur]
}

func (mr *MR) Check(name string) bool {
	for _, m := range mr.names {
		if m == name {
			return true
		}
	}
	return false
}

func (mr *MR) Register(name string) {
	if !mr.Check(name) {
		mr.names = append(mr.names, name)
	}
	mr.db.Update(func(tx *bolt.Tx) error {
		tx.Bucket(mr.namespace).Put([]byte(name), []byte(""))
		return nil
	})
}

func (mr *MR) KnownMastersCount() (r int) {
	mr.db.View(func(tx *bolt.Tx) error {
		r = tx.Bucket(mr.namespace).Stats().KeyN
		return nil
	})
	return
}

func (mr *MR) ClearKnownMasters() {
	mr.db.Update(func(tx *bolt.Tx) error {
		tx.DeleteBucket(mr.namespace)
		tx.CreateBucket(mr.namespace)
		return nil
	})
}
