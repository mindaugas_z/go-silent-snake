package tcf

import (
	"context"
	"errors"
	"sync"
	"time"
)

var (
	ErrAllDead = errors.New("All methods are dead")
)

type Response struct {
	Body interface{}
	Err  error
}

func Waterfall(d time.Duration, fns ...func() *Response) (interface{}, error) {
	if len(fns) <= 0 {
		return "", ErrAllDead
	}
	ctx, cfn := context.WithTimeout(context.Background(), d)
	defer cfn()
	ch := make(chan *Response)
	go func() {
		ch <- fns[0]()
	}()
	select {
	case response := <-ch:
		return response.Body, response.Err
	case <-ctx.Done():
		return Waterfall(d, fns[1:]...)
	}

}

func Simple(d time.Duration, fn1 func() *Response, fn2 func() *Response) (interface{}, error) {
	ch := make(chan *Response)
	defer close(ch)
	w := make(chan struct{})
	go func() {
		defer func() { recover() }()
		close(w)
		select {
		case ch <- fn1():
		}
	}()
	<-w
	l := false
L:
	select {
	case response := <-ch:
		return response.Body, response.Err
	case <-time.After(d):
		if !l {
			l = true
			w := make(chan struct{})
			go func() {
				defer func() { recover() }()
				close(w)
				select {
				case ch <- fn2():
				}
			}()
			<-w
			goto L
		}
	}

	return nil, ErrAllDead
}

func MutlQuery(fns ...func() interface{}) interface{} {

	results := make(chan interface{})

	done := make(chan struct{})

	var wg sync.WaitGroup

	for _, fn := range fns {
		wg.Add(1)
		go func(wg *sync.WaitGroup, fn func() interface{}) {
			defer wg.Done()
			select {
			case <-done:
			case results <- fn():
			}
		}(&wg, fn)
	}
	result := <-results

	close(done)

	go func(wg *sync.WaitGroup) {
		wg.Wait()
		close(results)
	}(&wg)

	return result
}
