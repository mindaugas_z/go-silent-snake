package tcf_test

import (
	"bitbucket.org/mindaugas_z/go-silent-snake/libs/tcf"
	"testing"
	"time"
)

func TestMain(t *testing.T) {

	resp, err := tcf.Waterfall(100*time.Millisecond,
		func() *tcf.Response {
			time.Sleep(110 * time.Millisecond)
			return &tcf.Response{"1st", nil}
		},
		func() *tcf.Response {
			time.Sleep(110 * time.Millisecond)
			return &tcf.Response{"2nd", nil}
		},
		func() *tcf.Response {
			time.Sleep(90 * time.Millisecond)
			return &tcf.Response{"3rd", nil}
		},
	)

	if err != nil {
		t.Fatal()
	}
	if resp.(string) != "3rd" {
		t.Fatal()
	}

	resp, err = tcf.Waterfall(200*time.Millisecond,
		func() *tcf.Response {
			time.Sleep(300 * time.Millisecond)
			return &tcf.Response{"1st", nil}
		},
		func() *tcf.Response {
			return &tcf.Response{"2nd", nil}
		},
	)
	if err != nil {
		t.Fatal()
	}
	if resp.(string) != "2nd" {
		t.Fatal()
	}

	resp, err = tcf.Waterfall(200*time.Millisecond,
		func() *tcf.Response {
			time.Sleep(300 * time.Millisecond)
			return &tcf.Response{"1st", nil}
		},
		func() *tcf.Response {
			time.Sleep(300 * time.Millisecond)
			return &tcf.Response{"2nd", nil}
		},
	)
	if resp.(string) != "" {
		t.Fatal()
	}
	if err != tcf.ErrAllDead {
		t.Fatal()
	}
}

func TestSimple(t *testing.T) {
	a, b := tcf.Simple(100*time.Millisecond,
		func() *tcf.Response {
			return &tcf.Response{"1st", nil}
		},
		func() *tcf.Response {
			return &tcf.Response{"2nd", nil}
		},
	)
	if b != nil {
		t.Fatal()
	}
	if a.(string) != "1st" {
		t.Fatal()
	}

	a, b = tcf.Simple(100*time.Millisecond,
		func() *tcf.Response {
			time.Sleep(150 * time.Millisecond)
			return &tcf.Response{"1st", nil}
		},
		func() *tcf.Response {
			time.Sleep(80 * time.Millisecond)
			return &tcf.Response{"2nd", nil}
		},
	)

	if b != nil {
		t.Fatal()
	}
	if a.(string) != "1st" {
		t.Fatal()
	}

	a, b = tcf.Simple(100*time.Millisecond,
		func() *tcf.Response {
			time.Sleep(150 * time.Millisecond)
			return &tcf.Response{"1st", nil}
		},
		func() *tcf.Response {
			return &tcf.Response{"2nd", nil}
		},
	)

	if b != nil {
		t.Fatal()
	}
	if a.(string) != "2nd" {
		t.Fatal()
	}

	a, b = tcf.Simple(100*time.Millisecond,
		func() *tcf.Response {
			time.Sleep(250 * time.Millisecond)
			return &tcf.Response{"1st", nil}
		},
		func() *tcf.Response {
			time.Sleep(250 * time.Millisecond)
			return &tcf.Response{"2nd", nil}
		},
	)

	if b != tcf.ErrAllDead {
		t.Fatal(b)
	}
	if a != nil {
		t.Fatal()
	}
}

func TestMultiQuery(t *testing.T) {

	fn1 := func() interface{} {
		time.Sleep(time.Second)
		return 1
	}
	fn2 := func() interface{} {
		return 2
	}

	result := tcf.MutlQuery(fn1, fn2)

	if result.(int) != 2 {
		t.Error()
	}

	fn3 := func() interface{} {
		return 1
	}
	fn4 := func() interface{} {
		time.Sleep(time.Second)
		return 2
	}
	result = tcf.MutlQuery(fn3, fn4)

	if result.(int) != 1 {
		t.Error()
	}
}
