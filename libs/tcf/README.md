# tcf

chain of methods with timeout

```go
	resp, err := tcf.Waterfall(100*time.Millisecond,
		func() *tcf.Response {
			time.Sleep(110 * time.Millisecond)
			return &tcf.Response{"1st", nil}
		},
		func() *tcf.Response {
			time.Sleep(110 * time.Millisecond)
			return &tcf.Response{"2nd", nil}
		},
		func() *tcf.Response {
			time.Sleep(90 * time.Millisecond)
			return &tcf.Response{"3rd", nil}
		},
	)

	fmt.Printf("%v == %v", resp, "3rd")
   
```

```go
	resp, _ := tcf.Simple(100*time.Millisecond,
		func() *tcf.Response {
			time.Sleep(150 * time.Millisecond)
			return &tcf.Response{"1st", nil}
		},
		func() *tcf.Response {
			time.Sleep(80 * time.Millisecond)
			return &tcf.Response{"2nd", nil}
		},
	)

	fmt.Printf("%v == %v", resp, "1st")

```

fastest wins
```go
var fns []func() interface{}
	fns = append(fns, func() interface{} {
		// context with timeout
		return 1
	})
	fns = append(fns, func() interface{} {
		// context with timeout
		return 2
	})
	fns = append(fns, func() interface{} {
		// context with timeout
		return 3
	})

	result := tcf.MutlQuery(fns...)
	fmt.Printf("%#v\n", result)
```