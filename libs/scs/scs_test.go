package scs

import (
	"testing"
	"time"
)

func TestMain(t *testing.T) {
	glq := New()

	glq.ResetAndSetBufferSize(10)

	stopfn := glq.Worker()

	var c int

	go func() {
		for i := 0; i < 10; i++ {
			glq.Send(func() {
				c++
			})
		}
	}()
	go func() {
		for i := 0; i < 10; i++ {
			glq.Send(func() {
				c++
			})
		}
	}()
	go func() {
		for i := 0; i < 10; i++ {
			glq.Send(func() {
				c++
			})
		}
	}()

	time.Sleep(1 * time.Second)
	if c != 30 {
		t.Fatal()
	}
	_ = glq.Chan()

	stopfn() // bug

	time.Sleep(100 * time.Millisecond)
}
