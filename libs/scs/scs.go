package scs

func New() *T {
	t := new(T)
	t.fnch = make(chan func())
	return t
}

type T struct {
	fnch chan func()
}

func (t *T) ResetAndSetBufferSize(size int64) {
	t.fnch = make(chan func(), size)
}

func (t *T) Send(fn func()) {
	t.fnch <- fn
}

func (t *T) Chan() chan func() {
	return t.fnch
}

func (t *T) Worker() (cl func()) {
	ch := make(chan struct{})
	go func() {
		for {
			select {
			case <-ch:
				return
			case fn, ok := <-t.fnch:
				if ok {
					fn()
				}
			}
		}
	}()
	return func() { close(ch) }
}
