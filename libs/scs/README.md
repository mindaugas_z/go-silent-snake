# Simple Commands Queue

```go
func main() {
	s := scs.New()
	s.ResetAndSetBufferSize(100)

    closeFn := s.Worker()
    
	s.Send(func() {
		println("1st")
    })
    
	s.Send(func() {
		println("2nd")
	})

	time.Sleep(1 * time.Second)
	closeFn()
}
```