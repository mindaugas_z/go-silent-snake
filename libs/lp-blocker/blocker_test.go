package lpblocker

import (
	"testing"
	"time"
)

func TestMainV1(t *testing.T) {
	b := New(true)

	var t1 bool

	go func() {
		b.Wait()
		t1 = true
	}()

	time.Sleep(100 * time.Millisecond)
	if t1 {
		t.Fatal()
	}
	b.Unlock()

	time.Sleep(100 * time.Millisecond)

	if !t1 {
		t.Fatal()
	}

	if b.Locked() {
		t.Fatal()
	}
	b.Relock()

	if !b.Locked() {
		t.Fatal()
	}

	b.Lock()
	var t2 bool
	go func() {
		b.WaitWithTimeout(100 * time.Millisecond)
		t2 = true
	}()
	time.Sleep(1000 * time.Millisecond)
	if !t2 {
		t.Fatal()
	}
}

func TestMainV2(t *testing.T) {
	b := New2(true)

	var t1 bool

	go func() {
		b.Wait()
		t1 = true
	}()

	time.Sleep(100 * time.Millisecond)
	if t1 {
		t.Fatal()
	}
	b.Unlock()

	time.Sleep(100 * time.Millisecond)

	if !t1 {
		t.Fatal()
	}

	if b.Locked() {
		t.Fatal()
	}
	b.Relock()

	if !b.Locked() {
		t.Fatal()
	}

	b.Lock()
	var t2 bool
	go func() {
		b.WaitWithTimeout(100 * time.Millisecond)
		t2 = true
	}()
	time.Sleep(300 * time.Millisecond)
	if !t2 {
		t.Fatal()
	}
}

func TestMain(t *testing.T) {
	v1 := New(false)
	v2 := New2(false)

	if v1.Locked() {
		t.Fatal()
	}
	if v2.Locked() {
		t.Fatal()
	}
}
