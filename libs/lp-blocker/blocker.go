package lpblocker

import (
	"sync"
	"time"
)

// Channels variant
func New(block bool) *T {
	t := new(T)
	t.ch = make(chan struct{})
	if !block {
		close(t.ch)
	}
	return t
}

type T struct {
	ch chan struct{}
}

func (t *T) Unlock() {
	select {
	case _, ok := <-t.ch:
		if !ok {
			return
		}
	default:
		close(t.ch)
	}
}

func (t *T) Lock() {
	t.ch = make(chan struct{})
}

func (t *T) Relock() {
	t.Unlock()
	t.Lock()
}

func (t *T) Locked() bool {
	select {
	case <-t.ch:
		return false
	default:
		return true
	}
}

func (t *T) Wait() {
	select {
	case <-t.ch:
	}
}

func (t *T) WaitWithTimeout(duration time.Duration) {
	select {
	case <-t.ch:
	case <-time.After(duration):
	}
}

// sync.Cond variant
func New2(block bool) *T2 {
	t := new(T2)
	t.block = block
	t.mu = &sync.Mutex{}
	t.cond = sync.NewCond(t.mu)
	return t
}

type T2 struct {
	block bool
	mu    *sync.Mutex
	cond  *sync.Cond
}

func (t *T2) Unlock() {
	t.mu.Lock()
	t.block = false
	t.cond.Broadcast()
	t.mu.Unlock()
}

func (t *T2) Lock() {
	t.mu.Lock()
	t.block = true
	t.mu.Unlock()
}

func (t *T2) Relock() {
	t.mu.Lock()
	t.block = true
	t.cond.Broadcast()
	t.mu.Unlock()
}

func (t *T2) Locked() bool {
	t.mu.Lock()
	defer t.mu.Unlock()
	return t.block
}

func (t *T2) Wait() {
	t.mu.Lock()
	if t.block {
		t.cond.Wait()
	}
	t.mu.Unlock()
}

func (t *T2) WaitWithTimeout(duration time.Duration) {
	ch := make(chan struct{})
	go func() {
		t.Wait()
		close(ch)
	}()
	select {
	case <-time.After(duration):
	case <-ch:
	}
}
