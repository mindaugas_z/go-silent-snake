# go routines [non] blocker

block with channels or lock all go routines at once

// just playing with channels


```go
func main() {
	// block by default
	blocker := lpblocker.New(true)
	go func() {
		println("before")
		blocker.Wait()
		println("after")
	}()
	
	time.Sleep(time.Second)

	blocker.Unlock()
	blocker.Wait()
	println("done.")

	time.Sleep(time.Second)
}
```
