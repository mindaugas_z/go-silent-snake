# bolt ses

golang boltdb streaming event store


```
API
bes.New()
bes.Play(fromUnixNano, toUnixNano, func(bes.LogMessage))
bes.PlayAndSubscribe(fromUnixNano, func(bes.LogMessage))
unixNanoId = bes.Store(bes.LogMessage)

bes.PlayWithPrefix(string,fromUnixNano, toUnixNano, func(bes.LogMessage))
bes.PlayAndSubscribeWithPrefix(string, fromUnixNano, func(bes.LogMessage))
unixNanoId = bes.StoreWithPrefix(string, bes.LogMessage)

bes.Delete(olderThanUnixNano)

bes.WriteRaw(bes.LogMessage)
bes.Notify()
bes.Last() bes.LogMessage
bes.First() bes.LogMessage
bes.Next(fromTime) bes.LogMessage
```


```go
db, _ := bolt.Open("db.db", 0755, nil)

es := bes.New(db, "mylog", 0)

es.Store("my event")

es.Play(fromUnixNano, toUnixNano, func(item bes.LogMessage) {
    // println(item.Data.(string)) 
})

es.PlayAndSubscribe(fromUnixNano, func(item bes.LogMessage) {
    // println(item.Data.(string))
})

es.Store("another event")

```
