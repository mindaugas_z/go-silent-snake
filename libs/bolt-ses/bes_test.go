package bes

import (
	"github.com/boltdb/bolt"
	"os"
	"reflect"
	"testing"
	"time"
)

func TestStreamingWithPrefix(t *testing.T) {
	db, _ := bolt.Open("db.db", 0755, nil)
	defer os.Remove("db.db")

	es := New(db, "logas", 0)

	es.StoreWithPrefix("p1", "a 1")
	es.StoreWithPrefix("p1", "a 2")

	es.StoreWithPrefix("p2", "b 1")
	es.StoreWithPrefix("p2", "b 2")

	var a, b []string

	sub1 := es.PlayAndSubscribeWithPrefix("p1", 0, func(i LogMessage) {
		a = append(a, i.Data.(string))
	})

	sub2 := es.PlayAndSubscribeWithPrefix("p2", 0, func(i LogMessage) {
		b = append(b, i.Data.(string))
	})

	time.Sleep(100 * time.Millisecond)
	if len(a) != 2 || len(b) != 2 {
		t.Fatalf("Bad message count")
	}

	es.StoreWithPrefix("p1", "b 3")
	es.StoreWithPrefix("p1", "b 4")
	es.StoreWithPrefix("p1", "b 5")
	es.StoreWithPrefix("p1", "b 6")

	time.Sleep(100 * time.Millisecond)
	if len(a) != 6 || len(b) != 2 {
		t.Fatalf("Bad message count")
	}

	es.StoreWithPrefix("p2", "a 3")
	es.StoreWithPrefix("p2", "a 4")
	es.StoreWithPrefix("p2", "a 5")

	time.Sleep(100 * time.Millisecond)
	if len(a) != 6 || len(b) != 5 {
		t.Fatalf("Bad message count %d %d", len(a), len(b))
	}
	sub1()
	sub2()

}

func TestStreamingWithPrefixAndWithout(t *testing.T) {
	db, _ := bolt.Open("db.db", 0755, nil)
	defer os.Remove("db.db")

	es := New(db, "logas", 0)

	es.StoreWithPrefix("p1", "a 1")
	es.StoreWithPrefix("p1", "a 2")

	es.StoreWithPrefix("p2", "b 1")
	es.StoreWithPrefix("p2", "b 2")

	var a, b, c []string

	sub1 := es.PlayAndSubscribeWithPrefix("p1", 0, func(i LogMessage) {
		a = append(a, i.Data.(string))
	})

	sub2 := es.PlayAndSubscribeWithPrefix("p2", 0, func(i LogMessage) {
		b = append(b, i.Data.(string))
	})

	sub3 := es.PlayAndSubscribe(0, func(i LogMessage) {
		c = append(c, i.Data.(string))
	})

	time.Sleep(100 * time.Millisecond)
	if len(a) != 2 || len(b) != 2 || len(c) != 4 {
		t.Fatalf("Bad message count")
	}

	es.StoreWithPrefix("p1", "b 3")
	es.StoreWithPrefix("p1", "b 4")
	es.StoreWithPrefix("p1", "b 5")
	es.StoreWithPrefix("p1", "b 6")

	time.Sleep(100 * time.Millisecond)
	if len(a) != 6 || len(b) != 2 || len(c) != 8 {
		t.Fatalf("Bad message count")
	}

	es.StoreWithPrefix("p2", "a 3")
	es.StoreWithPrefix("p2", "a 4")
	es.StoreWithPrefix("p2", "a 5")

	time.Sleep(100 * time.Millisecond)
	if len(a) != 6 || len(b) != 5 || len(c) != 11 {
		t.Fatalf("Bad message count")
	}
	sub1()
	sub2()
	sub3()
}

func TestReplayWithPrefix(t *testing.T) {
	db, _ := bolt.Open("db.db", 0755, nil)
	defer os.Remove("db.db")

	es := New(db, "logas", 0)

	es.StoreWithPrefix("p1", "a 1")
	es.StoreWithPrefix("p1", "a 2")

	es.StoreWithPrefix("p2", "b 1")
	es.StoreWithPrefix("p2", "b 2")

	es.Store("b 3")
	es.Store("a 3")

	var a []string

	es.PlayWithPrefix("p1", 0, int64(1<<63-1), func(i LogMessage) {
		a = append(a, i.Data.(string))
	})

	time.Sleep(100 * time.Millisecond)
	if len(a) != 2 {
		t.Fatalf("Bad message count")
	}

}

func TestStreaming(t *testing.T) {
	db, _ := bolt.Open("db.db", 0755, nil)
	defer os.Remove("db.db")

	es := New(db, "logas", 0)

	es.StoreWithPrefix("p1", "a 1")
	es.StoreWithPrefix("p1", "a 2")

	es.Store("b 1")
	es.Store("b 2")

	var a []string

	unsub1 := es.PlayAndSubscribe(0, func(i LogMessage) {
		a = append(a, i.Data.(string))
	})

	time.Sleep(100 * time.Millisecond)
	if len(a) != 4 {
		t.Fatalf("Bad message count")
	}

	es.StoreWithPrefix("p1", "b 3")
	es.StoreWithPrefix("p1", "b 4")
	es.StoreWithPrefix("p1", "b 5")
	es.StoreWithPrefix("p1", "b 6")

	time.Sleep(100 * time.Millisecond)
	if len(a) != 8 {
		t.Fatalf("Bad message count")
	}

	es.StoreWithPrefix("p2", "a 3")
	es.StoreWithPrefix("p2", "a 4")
	es.StoreWithPrefix("p2", "a 5")

	time.Sleep(100 * time.Millisecond)
	if len(a) != 11 {
		t.Fatalf("Bad message count")
	}

	unsub1()
}

func TestReplay(t *testing.T) {
	db, _ := bolt.Open("db.db", 0755, nil)
	defer os.Remove("db.db")

	es := New(db, "logas", 0)

	es.StoreWithPrefix("p1", "a 1")
	es.StoreWithPrefix("p1", "a 2")

	es.StoreWithPrefix("p2", "b 1")
	es.StoreWithPrefix("p2", "b 2")

	var a []string

	es.Play(0, int64(1<<63-1), func(i LogMessage) {
		a = append(a, i.Data.(string))
	})

	time.Sleep(100 * time.Millisecond)
	if len(a) != 4 {
		t.Fatalf("Bad message count")
	}
}

func TestReplaySmallRange(t *testing.T) {
	db, _ := bolt.Open("db.db", 0755, nil)
	defer os.Remove("db.db")

	es := New(db, "logas", 0)

	id1 := es.StoreWithPrefix("p1", "a 1")
	es.StoreWithPrefix("p1", "a 2")

	id2 := es.StoreWithPrefix("p2", "b 1")
	es.StoreWithPrefix("p2", "b 2")

	var a []string

	es.Play(id1, id2, func(i LogMessage) {
		a = append(a, i.Data.(string))
	})
	time.Sleep(100 * time.Millisecond)

	// 2 because we already know about 1st msg so play next
	if len(a) != 2 {
		t.Fatalf("Bad message count %d", len(a))
	}
}

func TestFuncs(t *testing.T) {
	db, _ := bolt.Open("db.db", 0755, nil)
	defer os.Remove("db.db")

	es := New(db, "logas", 0)

	id1 := es.StoreWithPrefix("p1", "a 1")
	id2 := es.StoreWithPrefix("p1", "a 1")

	if es.Last().ID != id2 {
		t.Fatalf("Wrong last ID")
	}
	if es.First().ID != id1 {
		t.Fatalf("Wrong first ID")
	}

	es.GetID = func(sid int64) int64 {
		return sid + 10
	}
	id3 := es.Store("a 2")

	if id3 != 10 {
		t.Fatalf("Bad ID")
	}

	if es.Check(0) != false {
		t.Fatalf("Failed check 0 ID should false %v", es.Check(0))
	}

	es.Delete(id3)
	if es.Check(id3) != false {
		t.Fatalf("Delete not works")
	}

	if es.Next(id1).ID != id2 {
		t.Fatalf("Expected true")
	}
}

func TestUnsub(t *testing.T) {
	db, _ := bolt.Open("db.db", 0755, nil)
	defer os.Remove("db.db")

	es := New(db, "logas", 0)

	es.StoreWithPrefix("p1", "a 1")
	es.StoreWithPrefix("p1", "a 2")

	es.StoreWithPrefix("p2", "b 1")
	es.StoreWithPrefix("p2", "b 2")

	var a, b []string

	sub1 := es.PlayAndSubscribeWithPrefix("p1", 0, func(i LogMessage) {
		a = append(a, i.Data.(string))
	})

	sub2 := es.PlayAndSubscribeWithPrefix("p2", 0, func(i LogMessage) {
		b = append(b, i.Data.(string))
	})

	time.Sleep(100 * time.Millisecond)
	sub1()
	sub2()
	if len(a) != 2 || len(b) != 2 {
		t.Fatalf("Bad message count")
	}

	es.StoreWithPrefix("p1", "b 3")
	es.StoreWithPrefix("p1", "b 4")
	es.StoreWithPrefix("p1", "b 5")
	es.StoreWithPrefix("p1", "b 6")

	time.Sleep(100 * time.Millisecond)
	if len(a) != 2 || len(b) != 2 {
		t.Fatalf("Bad message count")
	}

	es.StoreWithPrefix("p2", "a 3")
	es.StoreWithPrefix("p2", "a 4")
	es.StoreWithPrefix("p2", "a 5")

	time.Sleep(100 * time.Millisecond)
	if len(a) != 2 || len(b) != 2 {
		t.Fatalf("Bad message count %d %d", len(a), len(b))
	}

}

func TestMain(t *testing.T) {
	m := LogMessage{ID: 0, Data: "", Prefix: ""}
	b := m.Encode()
	var m2 LogMessage
	m2.Decode(b)
	reflect.DeepEqual(m, m2)

}
