# mwl

```go
func main() {
	m := mwl.New()
	m.SetLen(1)

	m.Set("value1")
	
	go func() {
		for {
			value := "value"
			if m.Check(value) {
				continue
			}

			//do job
			
			m.Set(value)
		}
	}
}
```