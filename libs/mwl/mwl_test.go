package mwl

import (
	"testing"
)

func TestMain(t *testing.T) {
	l := New()
	l.SetLen(3)

	l.Set("A")
	l.Set("B")
	l.Set("C")
	l.Set("D")
	l.Set("E")

	if l.Check("A") {
		t.Error()
	}
	if !l.Check("E") {
		t.Error()
	}
	l.SetLen(1)
	l.Set("F")
	if l.Check("E") {
		t.Error()
	}

	l.Set(5)
	if !l.Check(5) {
		t.Error()
	}
	l.Set(5)
	if !l.Check(5) {
		t.Error()
	}
	l.SetLen(0)
	l.Set(0)
	l.Set(1)
	if !l.Check(1) {
		t.Error()
	}
}
