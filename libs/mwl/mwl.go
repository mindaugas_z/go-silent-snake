package mwl

import (
	"container/list"
	"sync"
	"sync/atomic"
)

func New() *T {
	t := new(T)

	t.l = list.New()
	t.ll = make(map[interface{}]struct{})

	t.mu = &sync.RWMutex{}

	return t
}

type T struct {
	l   *list.List
	ll  map[interface{}]struct{}
	mu  *sync.RWMutex
	len uint64
}

func (t *T) SetLen(len uint64) {
	atomic.StoreUint64(&t.len, len)
}

func (t *T) Set(value interface{}) {
	t.mu.Lock()
	defer t.mu.Unlock()

	t.remove()

	t.l.PushBack(value)
	t.ll[value] = struct{}{}
}

func (t *T) Check(value interface{}) bool {
	t.mu.RLock()
	_, ok := t.ll[value]
	t.mu.RUnlock()
	return ok
}

func (t *T) remove() {
	for atomic.LoadUint64(&t.len) <= uint64(t.l.Len()) {
		el := t.l.Front()
		if el == nil {
			return
		}
		delete(t.ll, el.Value)
		t.l.Remove(el)
	}
}
