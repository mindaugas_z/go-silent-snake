package toe

import (
	"testing"
	"time"
)

func TestMain(t *testing.T) {

	var r1 bool
	var r2 bool

	e := New(100*time.Millisecond, func(tmi bool) {
		if tmi {
			r1 = true
		}
	})

	e.Wait()

	if !r1 {
		t.Fatal()
	}

	e1 := New(1000*time.Millisecond, func(tmi bool) {
		if tmi {
			r2 = true
		}
	})
	go func() {
		time.Sleep(800 * time.Millisecond)
		e1.Reset()
		time.Sleep(800 * time.Millisecond)
		e1.Event()
	}()
	e1.Wait()
	if r2 {
		t.Fatal()
	}

	time.Sleep(1 * time.Second)
}
