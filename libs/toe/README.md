# go-toe

Run func on timeout or on event


```
func main() {
  e := toe.New(1*time.Second, func(t bool) {
    fmt.Println("timeout or event", t)
  })
  _ = e

  // mq.subscribe(ch, func() { e.Event() })
  // e.Event()
}
```
