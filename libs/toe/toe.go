package toe

import (
	"time"
)

func New(d time.Duration, fn func(bool)) *T {
	t := new(T)
	t.d = d
	t.F = fn
	t.timer = time.NewTimer(d)
	t.event = make(chan struct{})
	go t.worker()
	return t
}

type T struct {
	d     time.Duration
	timer *time.Timer
	event chan struct{}
	F     func(bool)
}

func (t *T) worker() {
	select {
	case <-t.timer.C:
		t.run(true)
	case <-t.event:
		t.run(false)
	}
}

func (t *T) run(to bool) {
	t.timer.Stop()
	t.close()
	t.F(to)
}

func (t *T) close() {
	if !closed(t.event) {
		close(t.event)
	}
}

func (t *T) Wait() {
	select {
	case <-t.event:
	}
}

func (t *T) Reset() {
	t.timer.Reset(t.d)
}

func (t *T) Event() {
	t.close()
}

func closed(ch chan struct{}) bool {
	select {
	case <-ch:
		return true
	default:
	}
	return false
}
