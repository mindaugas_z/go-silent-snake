package qs

import (
	"sync"
	"testing"
	"time"
)

func TestMain(t *testing.T) {
	q := New()

	var s []string
	var mu sync.Mutex

	q.Subscribe("test", func(i Item) {
		mu.Lock()
		s = append(s, i.(string))
		mu.Unlock()
	})
	q.Subscribe("test", func(i Item) {
		mu.Lock()
		s = append(s, i.(string))
		mu.Unlock()
	})
	q.Publish("test", "p1")
	q.Publish("test", "p2")
	q.Publish("test", "p3")

	time.Sleep(100 * time.Millisecond)
	if len(s) != 3 {
		t.Fatal()
	}
}

func TestUnsubscribeFail(t *testing.T) {
	q := New()

	var s []string

	q.Subscribe("test", func(i Item) {
		s = append(s, i.(string))
	})
	q.Subscribe("test", func(i Item) {
		s = append(s, i.(string))
	})
	q.Unsubscribe("test")

	time.Sleep(1000 * time.Millisecond)

	q.Publish("test", "p4")

	time.Sleep(100 * time.Millisecond)

	if len(s) != 0 {
		t.Fatal()
	}

	q.Publish("test", "p5")
	time.Sleep(100 * time.Millisecond)

	if len(s) != 0 {
		t.Fatal()
	}

}
