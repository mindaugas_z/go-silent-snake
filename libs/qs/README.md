# go-qs

Queue Subscribe

```
    q := qs.New()

	q.Subscribe("test", func(i qs.Item) {
		println(i.(string))
	})

	q.Publish("test", "1st")
	q.Publish("test", "2nd")
```