package sfr

import (
	"reflect"
	"sync"
)

func New(MaxParamLen int) *T {
	t := new(T)
	t.fns = make(map[string]interface{})
	t.mu = &sync.RWMutex{}
	t.mpl = MaxParamLen
	return t
}

type T struct {
	fns map[string]interface{}
	mu  *sync.RWMutex
	mpl int
}

func (t *T) Run(s string) {
	cmd := t.sw(&s)

	t.mu.RLock()
	fn, ok := t.fns[cmd]
	t.mu.RUnlock()

	if !ok {
		return
	}

	funcArgs := reflect.ValueOf(fn).Type().NumIn()
	funcValue := reflect.ValueOf(fn)

	v := make([]reflect.Value, funcArgs)

	if funcArgs == 1 {
		v[0] = reflect.ValueOf(s)
	}
	if funcArgs > 1 {
		for i := 0; i < funcArgs-1; i++ {
			v[i] = reflect.ValueOf(t.sw(&s))
		}
		v[funcArgs-1] = reflect.ValueOf(s)
	}

	funcValue.Call(v)
	// tw(&s)
}

func (t *T) Bind(cmd string, fn interface{}) {
	t.mu.Lock()
	t.fns[cmd] = fn
	t.mu.Unlock()
}

func (t *T) sw(s *string) string {
	l, rt, i := t.mpl, "", 0

	for _, v := range *s {
		if v != ' ' {
			rt = rt + string(v)
		} else {
			break
		}
		i++
		if i >= l {
			break
		}
	}
	if len(*s) <= i {
		*s = ""
		return rt
	}
	*s = (*s)[i+1:]
	return rt
}
