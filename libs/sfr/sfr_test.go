package sfr

import (
	"strings"
	"testing"
)

func TestMain(t *testing.T) {
	mpl := 100
	c := New(mpl)

	var r1 string
	var r2 string
	var r3 string
	var r4 string
	var r5 string
	var r6 string
	var r7 string
	var r8 string

	c.Bind("1st", func() {
		r1 = ""
	})

	c.Bind("2nd", func(s string) {
		r2 = s
	})

	c.Bind("3rd", func(a, b string) {
		r3 = a
		r4 = b
	})

	c.Bind("4rd", func(a, b string) {
		r5 = a
		r6 = b
	})

	c.Bind("5th", func(a, b string) {
		r7 = a
		r8 = b
	})

	c.Bind("6", func(a, b string) {
		if len(a) != mpl {
			t.Fatal()
		}
	})

	c.Run("1st params 1")
	c.Run("2nd params 2")
	c.Run("3rd params 3")
	c.Run("4rd params 4 5")
	c.Run("5th")
	toolongparam := strings.Repeat("a", 2000)
	c.Run("6 " + toolongparam + " p2")

	if r1 != "" {
		t.Fatal()
	}
	if r2 != "params 2" {
		t.Fatal()
	}
	if r3 != "params" && r4 != "3" {
		t.Fatal()
	}

	if r5 != "params" && r6 != "4 5" {
		t.Fatal()
	}

	if r7 != "" && r8 != "" {
		t.Fatal()
	}

	c.Run("non exists")
}
