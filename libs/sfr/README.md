```go
func main() {
    r := sfr.New()

    r.Bind("cmd", func(param1, param2, other string) {
        println("param1:", param1, "param2:", param2, "param3:", other)
    })

    r.Run("cmd with-param-1 with-param-2 and all other text")
}
```