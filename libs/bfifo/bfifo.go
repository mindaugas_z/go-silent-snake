package bfifo

import (
	"bytes"
)

func New() *T {
	t := new(T)
	t.len = 3
	return t
}

type T struct {
	m   []byte
	len int
}

func (t *T) SetBufferLen(l int) {
	t.len = l
}

func (t *T) Add(s byte) {
	t.m = append(t.m, s)
	if len(t.m) > t.len {
		bs := make([]byte, t.len)
		bs = t.m[len(t.m)-t.len:]
		t.m = bs
	}
}

func (t *T) Value(b []byte) []byte {
	bl := len(b)
	bln := len(t.m) - bl
	if len(t.m) >= bl || bln >= 0 {
		return t.m[bln:len(t.m)]
	}
	return t.m
}

func (t *T) Compare(v []byte) bool {
	return bytes.Compare(t.Value(v), v) == 0
}
