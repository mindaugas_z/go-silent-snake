package bfifo

import (
	"testing"
)

func TestMain(t *testing.T) {
	bf := New()
	bf.SetBufferLen(10)

	var str []byte = []byte("nopnopnop:magic:word word word:magic:another wo:magic:rds here:magic:nopnopnop")
	var magic = ":magic:"
	var mlen = len(magic)

	slice := [][]byte{}
	word := []byte{}

	m := false
	ab := false

	for _, s := range str {
		_ = magic
		bf.Add(s)

		word = append(word, s)

		if bf.Compare([]byte(magic)) {
			if m {
				slice = append(slice, word[:len(word)-mlen])
			}
			word = []byte(nil)
			m = true
		}
		if bf.Compare([]byte("12345678901234567890")) {
			ab = true
		}
	}

	if ab {
		t.Fatal()
	}

	if len(slice) != 3 {
		t.Fatal()
	}

	if string(slice[0]) != "word word word" {
		t.Fatal()
	}
	if string(slice[1]) != "another wo" {
		t.Fatal()
	}
	if string(slice[2]) != "rds here" {
		t.Fatal()
	}
}
