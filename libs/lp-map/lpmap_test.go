package lpmap

import (
	"testing"
	// "time"
)

func TestMain(t *testing.T) {
	m := New()

	if m.Exists("v1") {
		t.Fatal()
	}

	if !m.Set("v1") {
		t.Fatal()
	}
	if m.Set("v1") {
		t.Fatal()
	}

	if !m.Exists("v1") {
		t.Fatal()
	}

	if !m.Remove("v1") {
		t.Fatal()
	}
	if m.Remove("v1") {
		t.Fatal()
	}

	m.Set("v2")
	m.Reset()
	if m.Remove("v2") {
		t.Fatal()
	}
	if m.Exists("v2") {
		t.Fatal()
	}
	if !m.Set("v2") {
		t.Fatal()
	}
}
