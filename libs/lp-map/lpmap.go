package lpmap

import (
	"sync"
)

func New() *Map {
	m := new(Map)
	m.mu = &sync.Mutex{}
	m.m = make(map[interface{}]struct{})
	return m
}

type Map struct {
	m  map[interface{}]struct{}
	mu *sync.Mutex
}

func (m *Map) Set(item interface{}) bool {
	m.mu.Lock()
	_, ok := m.m[item]
	if !ok {
		m.m[item] = struct{}{}
	}
	m.mu.Unlock()
	return !ok
}

func (m *Map) Exists(item interface{}) bool {
	m.mu.Lock()
	_, ok := m.m[item]
	m.mu.Unlock()
	return ok
}

func (m *Map) Remove(item interface{}) bool {
	m.mu.Lock()
	_, ok := m.m[item]
	if ok {
		delete(m.m, item)
	}
	m.mu.Unlock()
	return ok
}

func (m *Map) Reset() {
	m.mu.Lock()
	m.m = make(map[interface{}]struct{})
	m.mu.Unlock()
}
