# go-lp-map

map value set

```go
func main() {
	m := lpmap.New()

	if m.Set("some-value") {
		println("do job")
	}

	if !m.Set("some-value") {
		println("dont do job")
    }
    
    if m.Exists("some-value") {
        // do something
        m.Delete("some-value")
	}
	
	// m.Reset()
}
```