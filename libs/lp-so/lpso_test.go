package lpso

import (
	"testing"
	"time"
)

func TestMain(t *testing.T) {
	so := New()

	if so.Check() {
		t.Fatal()
	}

	if !so.Set("c1") {
		t.Fatal()
	}
	if !so.Check() {
		t.Fatal()
	}
	if so.Set("c2") {
		t.Fatal()
	}

	so.ChangeValue("c3")
	if so.Get().(string) != "c3" {
		t.Fatal()
	}

	so.Reset()

	if !so.Set("c2") {
		t.Fatal()
	}

	so.Reset()

	if !so.WaitWithTimeout(100 * time.Millisecond) {
		t.Fatal()
	}
	so.Reset()
	go func() {
		if so.WaitWithTimeout(1 * time.Second) {
			t.Fatal()
		}
	}()
	so.Set("dont timeout")

	so.Reset()

	var s bool
	go func() {
		so.Wait()
		s = true
	}()
	so.Set("dont wait forever")
	time.Sleep(1 * time.Second)
	if !s {
		t.Fatal()
	}
}
