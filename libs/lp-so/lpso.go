package lpso

import (
	"sync"
	"time"
)

func New() *SetOnce {
	so := new(SetOnce)
	so.mu = &sync.RWMutex{}
	so.ch = make(chan struct{})
	return so
}

type SetOnce struct {
	value interface{}
	mu    *sync.RWMutex
	ch    chan struct{}
}

func (so *SetOnce) Check() bool {
	so.mu.RLock()
	defer so.mu.RUnlock()
	return so.value != nil
}

func (so *SetOnce) Set(v string) bool {
	so.mu.Lock()
	defer so.mu.Unlock()
	if so.value == nil {
		close(so.ch)
		so.value = v
		return true
	}
	return false
}

func (so *SetOnce) Get() interface{} {
	so.mu.RLock()
	defer so.mu.RUnlock()
	return so.value
}

func (so *SetOnce) Reset() {
	so.mu.Lock()
	so.ch = make(chan struct{})
	so.value = nil
	so.mu.Unlock()
}

func (so *SetOnce) ChangeValue(v interface{}) {
	so.mu.Lock()
	so.value = v
	so.mu.Unlock()
}

func (so *SetOnce) Wait() {
	<-so.ch
}

// Returns True on timeout
func (so *SetOnce) WaitWithTimeout(duration time.Duration) bool {
	select {
	case <-so.ch:
		return false
	case <-time.After(duration):
		return true
	}
}
