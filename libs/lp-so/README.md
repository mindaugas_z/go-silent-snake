# go-lp-so

golang set value once 


```go
func main() {
	so := lpso.New()

	if so.Set("content") {
		println("do something")
	}
	if so.Set("another content") {
		println("another job")
	} else {
        println("value:", so.Get().(string))
    }

	so.Reset()

	if so.Set("another content") {
		println("another job:", so.Get().(string))
	}
}
```
