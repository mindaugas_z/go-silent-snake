package fnobserver

import (
	"testing"
	// "time"
)

func TestWithoutParams(t *testing.T) {
	fo := New()

	var c int
	fo.Add(func() {
		c++
	})
	fo.Run()

	if c != 1 {
		t.Fatal()
	}
	fo.Run()
	if c != 2 {
		t.Fatal()
	}
}

func TestWithParams(t *testing.T) {
	fo := New()
	var c int
	fo.Add(func(cc int) {
		c += cc
	})
	fo.Run(10)
	if c != 10 {
		t.Fatal()
	}
	fo.Run(20)
	if c != 30 {
		t.Fatal()
	}
}

func TestFailIncorrectParamsCount(t *testing.T) {
	defer func() {
		if r := recover(); r == nil {
			t.Fatal()
		}
	}()
	fo := New()
	fo.Add(func(v string) {})
	fo.Run()
}
