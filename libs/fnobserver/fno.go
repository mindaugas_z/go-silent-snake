package fnobserver

import (
	"reflect"
	"sync"
)

func New() *T {
	t := new(T)
	t.mu = &sync.Mutex{}
	return t
}

type T struct {
	fns []interface{}
	mu  *sync.Mutex
}

func (t *T) Add(fn interface{}) {
	t.mu.Lock()
	t.fns = append(t.fns, fn)
	t.mu.Unlock()
}

func (t *T) Run(params ...interface{}) {
	t.mu.Lock()
	defer t.mu.Unlock()

	for _, fn := range t.fns {
		t.run(fn, params...)
	}
}
func (t *T) run(fn interface{}, params ...interface{}) {
	funcArgs := reflect.ValueOf(fn).Type().NumIn()
	funcValue := reflect.ValueOf(fn)

	v := make([]reflect.Value, funcArgs)
	for i := 0; i <= funcArgs-1; i++ {
		v[i] = reflect.ValueOf(params[i])
	}

	funcValue.Call(v)
}
