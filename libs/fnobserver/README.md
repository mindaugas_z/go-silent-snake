# Unsafe FuncObserver

## array of dynamic functions


```go
func main() {
	type1 := fnobserver.New()

	type1.Add(func(s string, b []byte) {
		println(s, string(b))
	})
	type1.Run("param1", []byte("param2"))

	type2 := fnobserver.New()
	type2.Add(func() {
		println("no params")
	})
	type2.Run()

}
```
