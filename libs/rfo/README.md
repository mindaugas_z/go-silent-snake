# Unsafe Run Func Once

```go
func main() {
	fns := rfo.New(10)

	go func() {
		for {
			fns.Run("1st payload", []byte("2nd payload"))
			time.Sleep(time.Second)
		}
	}()

	fns.Add(func(s string, b []byte) {
		println(s, string(b))
	})
	time.Sleep(time.Second)
	fns.Add(func(s string, b []byte) {
		println("other:", s, string(b))
	})

	time.Sleep(2 * time.Second)
}
```