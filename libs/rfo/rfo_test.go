package rfo

import (
	"testing"
	"time"
)

func TestWithoutParams(t *testing.T) {
	r := New(1)
	go func() {
		for {
			r.Run()
			time.Sleep(100 * time.Millisecond)
		}
	}()

	var c int

	r.Add(func() {
		c++
	})

	time.Sleep(500 * time.Millisecond)
	if c != 1 {
		t.Fatal()
	}

	r.Add(func() {
		c++
	})
	time.Sleep(500 * time.Millisecond)
	if c != 2 {
		t.Fatal()
	}
}

func TestWithParams(t *testing.T) {
	r := New(1)
	go func() {
		for {
			r.Run("param")
			time.Sleep(100 * time.Millisecond)
		}
	}()

	var s string

	r.Add(func(p string) {
		s = "p " + p
	})

	time.Sleep(500 * time.Millisecond)
	if s != "p param" {
		t.Fatal()
	}

	r.Add(func(p string) {
		s = p
	})
	time.Sleep(500 * time.Millisecond)

	if s != "param" {
		t.Fatal()
	}
}

func TestFail(t *testing.T) {

	r := New(1)

	go func() {
		defer func() {
			if r := recover(); r == nil {
				t.Fatal()
			}
		}()
		for {
			r.Run()
			time.Sleep(100 * time.Millisecond)
		}
	}()

	r.Add(func(s string) {})

}
