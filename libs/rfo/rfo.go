package rfo

import (
	"reflect"
	"sync/atomic"
)

func New(buffer int) *T {
	t := new(T)
	t.ch = make(chan interface{}, buffer)
	return t
}

type T struct {
	len int32
	ch  chan interface{}
}

func (t *T) Add(fn interface{}) {
	t.ch <- fn
	atomic.AddInt32(&t.len, 1)
}

func (t *T) run(fn interface{}, params ...interface{}) {
	funcArgs := reflect.ValueOf(fn).Type().NumIn()
	funcValue := reflect.ValueOf(fn)

	v := make([]reflect.Value, funcArgs)
	for i := 0; i <= funcArgs-1; i++ {
		v[i] = reflect.ValueOf(params[i])
	}

	funcValue.Call(v)
}

func (t *T) Run(params ...interface{}) {
	if atomic.LoadInt32(&t.len) == 0 {
		return
	}
FOR1:
	for {
		select {
		case fn, ok := <-t.ch:
			if ok {
				atomic.AddInt32(&t.len, -1)
				t.run(fn, params...)
			}
			continue FOR1
		default:
			break FOR1
		}
	}
}
