# go-deq

Double Evented Queue


```
func main() {

  unsub := deq.Subscribe(func(s deq.Item) {
    fmt.Println("got message", s.(string))
  })

  go func() {
    time.Sleep(10 * time.Second)
    unsub()
    deq.Subscribe(func(s deq.Item) {
      fmt.Println("[resub] got message", s.(string))
    })
  }()

  deq.PCap = 5
  // call on deq.LCap limit
  deq.PC = func(c int) {
    fmt.Println("Primary queue limit reached: ", c)
  }

  deq.SCap = 3
  // call on deq.GCap limit
  deq.SC = func(c int) {
    fmt.Println("Secondary queue limit reached: ", c)
  }

  // mq.subscribe(fn(msg) { deq.WriteSecondary(msg) })
  go func() {
    for {
      time.Sleep(1 * time.Second)
      deq.WriteSecondary(fmt.Sprintf("global-%d", time.Now().Unix()))
    }
  }()

  // read from local file
  go func() {
    for i := 0; i <= 10; i++ {
      time.Sleep(500 * time.Millisecond)
      deq.WritePrimary(fmt.Sprintf("local-%d", i))

    }
    // local file parse done, switch to secondary queue
    deq.Switch()
  }()

  // run forever
  runtime.Goexit()
}
```


```go
func main() {
  var lt = deq.New()

  lt.WritePrimary(func() { println("fn 1") })
  lt.WritePrimary(func() { println("fn 2") })
  lt.WritePrimary(func() { println("fn 3") })

  for i, e := lt.ProcessPrimary(); e == nil; i, e = lt.ProcessPrimary() {
    i.(func())()
  }
}

```
