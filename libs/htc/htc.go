package htc

import (
	"context"
	"io"
	"sync"
	"sync/atomic"
)

func New() *T {
	t := new(T)
	t.d = [][]byte{}
	t.mu = &sync.RWMutex{}
	t.blocker = &sync.Mutex{}
	t.cond = sync.NewCond(t.blocker)
	return t
}

type T struct {
	d   [][]byte
	cnt int32

	eof bool
	mu  *sync.RWMutex

	blocker *sync.Mutex
	cond    *sync.Cond
}

func (t *T) Done() {
	t.blocker.Lock()
	t.eof = true
	t.blocker.Unlock()
}

func (t *T) WriteTo(w io.Writer) {
	ch, cfn := t.reader()
	defer func() {
		cfn()
	}()
	for b := range ch {
		_, e := w.Write(b)
		if e != nil {
			return
		}
	}
}

func (t *T) reader() (chan []byte, context.CancelFunc) {
	ch := make(chan []byte)
	var num int32 = 0
	fln := func() int {
		t.mu.RLock()
		defer t.mu.RUnlock()
		return len(t.d)
	}
	for fln() == 0 {
		t.blocker.Lock()
		t.cond.Wait()
		t.blocker.Unlock()
	}
	ctx, cfn := context.WithCancel(context.TODO())
	fn := func() []byte {
		t.mu.RLock()
		defer t.mu.RUnlock()
		return t.d[num]
	}
	go func() {
		defer func() {
			close(ch)
		}()
		for {

			pos := atomic.LoadInt32(&t.cnt)
			for pos > num {
				select {
				case ch <- fn():
				case <-ctx.Done():
					return
				}
				num++
			}
			t.blocker.Lock()
			if t.eof == true && atomic.LoadInt32(&t.cnt) == num {
				t.blocker.Unlock()
				return
			}
			if t.eof == false {
				t.cond.Wait()
			}
			t.blocker.Unlock()
		}

	}()
	return ch, cfn
}

func (t *T) Write(chunk []byte) (n int, err error) {
	t.mu.Lock()
	t.d = append(t.d, chunk)
	t.mu.Unlock()
	atomic.AddInt32(&t.cnt, 1)

	t.blocker.Lock()
	t.cond.Broadcast()
	t.blocker.Unlock()

	return len(chunk), nil
}

// im not sure about this but ..
func (t *T) Close() {
	t.d = nil
	t.cond = nil
	t.blocker = nil
	t.mu = nil
}
