# streaming cache

http streaming cache

```go
func main() {

	t := htc.New()

	ht := http.Client{}
	r, _ := ht.Get("http://bigFile.dat")
	go func() {
		io.Copy(t, r.Body)
		r.Body.Close()
		t.Done()
	}()

	http.HandleFunc("/out.dat", func(w http.ResponseWriter, r *http.Request) {
		//TODO: copy headers
		t.WriteTo(w)
	})

	http.ListenAndServe(":9002", nil)

	runtime.Goexit()
}
```
