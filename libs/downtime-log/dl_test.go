package DowntimeLog

import (
	"github.com/boltdb/bolt"
	"os"
	"testing"
	"time"
)

func initDB() *bolt.DB {
	db, _ := bolt.Open("db.db", 0755, nil)
	return db
}
func TestMain(t *testing.T) {
	defer os.Remove("db.db")
	dl := New(initDB(), "namespace")
	if dl.Count() > 0 {
		t.Fatal()
	}
	var runs int
	dl.Run(func(di DowntimeLogItem) {
		runs++
	})
	if runs > 0 {
		t.Fatal()
	}
	dl.Register(1, 2)

	dl.Run(func(di DowntimeLogItem) {
		runs++
		dl.Delete(di.ID)
	})
	if runs <= 0 {
		t.Fatal()
	}
	if dl.Count() > 0 {
		t.Fatal()
	}
	dl.Register(1, 3)
	dl.Flush()
	if dl.Count() > 0 {
		t.Fatal()
	}

	if dl.LastSeen() != -1 {
		t.Fatal()
	}
	dl.Flush()
	dl.Register(1, 3)
	if dl.Last().From != 1 && dl.Last().To != 3 {
		t.Fatal()
	}

	dl.StartHeartbeat(100 * time.Millisecond)
	time.Sleep(1 * time.Second)
	ls := dl.LastSeen()
	ns := time.Now().UnixNano()
	if ns-ls > int64(200*time.Millisecond) {
		t.Fatal()
	}
	dl.Stop()
}
