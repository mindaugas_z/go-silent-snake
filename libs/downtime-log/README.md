# go-downtime-log

Downtime log


```go
func main() {

	db, _ := bolt.Open("db.db", 0755, nil)

	log := DowntimeLog.New(db, "downtime-log")

	lastSeen := log.LastSeen()
	log.StartHeartbeat(100 * time.Millisecond)

	lastID := time.Now().UnixNano()
	if lastSeen != -1 {
		lastID = lastSeen

		log.Register(lastID, time.Now().UnixNano())
	}

	log.Run(func(i DowntimeLog.DowntimeLogItem) {
		fmt.Println(i.ID, i.From, i.To)
		// log.Delete(i.ID)
	})

	// log.Flush()

	time.Sleep(10 * time.Second)
}
```