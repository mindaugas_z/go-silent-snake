package DowntimeLog

import (
	"encoding/binary"
	"encoding/json"
	"github.com/boltdb/bolt"
	"time"
)

func New(db *bolt.DB, namespace string) *DowntimeLog {
	dl := new(DowntimeLog)
	dl.db = db
	dl.namespace = []byte(namespace)
	dl.hbNamespace = []byte(namespace + "-hb")
	dl.db.Update(func(tx *bolt.Tx) error {
		tx.CreateBucketIfNotExists(dl.namespace)
		tx.CreateBucketIfNotExists(dl.hbNamespace)
		return nil
	})
	return dl
}

type DowntimeLogItem struct {
	ID   uint64
	From int64
	To   int64
}

func (dli *DowntimeLogItem) encode() []byte {
	b, _ := json.Marshal(dli)
	return b
}

func (dli *DowntimeLogItem) decode(v []byte) {
	json.Unmarshal(v, dli)
}

type heartbeatItem struct {
	Time int64
}

func (hbi *heartbeatItem) encode() []byte {
	b, _ := json.Marshal(hbi)
	return b
}

func (hbi *heartbeatItem) decode(v []byte) {
	err := json.Unmarshal(v, hbi)
	if err != nil {
		hbi.Time = -1
	}
}

// Downtime Registrator
type DowntimeLog struct {
	db          *bolt.DB
	namespace   []byte
	hbNamespace []byte
	running     chan struct{}
}

func (dl *DowntimeLog) StartHeartbeat(duration time.Duration) {
	dl.running = make(chan struct{})
	timer := time.NewTicker(duration)
	go func() {
		for {
			select {
			case <-dl.running:
				return
			case <-timer.C:
				dl.db.Update(func(tx *bolt.Tx) error {
					var hbi heartbeatItem
					hbi.Time = time.Now().UnixNano()
					bucket := tx.Bucket(dl.hbNamespace)
					bucket.Put([]byte("seen"), hbi.encode())
					return nil
				})
				time.Sleep(duration)
			}
		}
	}()
}

func (dl *DowntimeLog) Stop() {
	select {
	case _, ok := <-dl.running:
		if !ok {
			return
		}
	default:
	}
	close(dl.running)
}

func (dl *DowntimeLog) LastSeen() int64 {
	var hbi heartbeatItem
	hbi.Time = -1
	dl.db.View(func(tx *bolt.Tx) error {
		bucket := tx.Bucket(dl.hbNamespace)
		v := bucket.Get([]byte("seen"))
		hbi.decode(clone(v))
		return nil
	})
	return hbi.Time
}

func (dl *DowntimeLog) Flush() {
	dl.db.Update(func(tx *bolt.Tx) error {
		tx.DeleteBucket(dl.namespace)
		tx.CreateBucket(dl.namespace)
		return nil
	})
}

func (dl *DowntimeLog) Count() (r int) {
	dl.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(dl.namespace)
		r = b.Stats().KeyN
		return nil
	})
	return
}

func (dl *DowntimeLog) Last() *DowntimeLogItem {
	var dli DowntimeLogItem
	dl.db.View(func(tx *bolt.Tx) error {
		_, v := tx.Bucket(dl.namespace).Cursor().Last()
		v1 := clone(v)
		dli.decode(v1)
		return nil
	})
	return &dli
}

func (dl *DowntimeLog) Register(from, to int64) {
	dl.db.Update(func(tx *bolt.Tx) error {
		var dli DowntimeLogItem

		dli.From = from
		dli.To = to

		bucket := tx.Bucket(dl.namespace)

		_num, _ := bucket.NextSequence()
		num := itob(int64(_num))
		dli.ID = _num

		bucket.Put(num, dli.encode())
		return nil
	})
}

func (dl *DowntimeLog) Run(f func(DowntimeLogItem)) {
	var fns []func()
	dl.db.View(func(tx *bolt.Tx) error {
		hist := tx.Bucket(dl.namespace)
		c := hist.Cursor()

		for k, v := c.First(); k != nil; k, v = c.Next() {
			v1 := clone(v)
			fns = append(fns, func() {
				v1 := v1
				var dli DowntimeLogItem
				dli.decode(v1)
				f(dli)
			})
		}

		return nil
	})
	for _, fn := range fns {
		fn()
	}
}

func (dl *DowntimeLog) Delete(id uint64) {
	dl.db.Update(func(tx *bolt.Tx) error {
		bucket := tx.Bucket(dl.namespace)
		return bucket.Delete(itob(int64(id)))
	})
}

func itob(v int64) []byte {
	b := make([]byte, 8)
	binary.BigEndian.PutUint64(b, uint64(v))
	return b
}

func clone(b []byte) []byte {
	r := make([]byte, len(b))
	copy(r, b)
	return r
}
