package ini

import (
	"os"
	"testing"
)

// for robot
func TestMain(t *testing.T) {

	defer os.Remove("config.test2.ini")

	c := New()
	c.Read("config.test.ini")

	if c.GetStringDefault("main", "c1___", "def") != "def" {
		t.Fatal()
	}
	if c.GetStringDefault("main", "c1", "w0 w1=w2 w3=w4") != "w0 w1=w2 w3=w4" {
		t.Fatal()
	}
	sv, e := c.GetString("main", "c1")
	if e != nil {
		t.Fatal()
	}
	if sv != "w0 w1=w2 w3=w4" {
		t.Fatal()
	}

	if c.GetBoolDefault("main", "a1___", true) != true {
		t.Fatal()
	}

	bv, e := c.GetBool("main", "a1")
	if e != nil {
		t.Fatal()
	}
	if bv != true {
		t.Fatal()
	}

	if c.GetIntDefault("main", "b1___", 1999991) != 1999991 {
		t.Fatal()
	}

	iv, e := c.GetInt("main", "b1")
	if e != nil {
		t.Fatal()
	}
	if iv != 9912345 {
		t.Fatal(iv)
	}

	if c.GetFloatDefault("skaiciai", "pirmas____", 1.9191) != 1.9191 {
		t.Fatal()
	}

	fv, e := c.GetFloat("skaiciai", "pirmas")
	if e != nil {
		t.Fatal()
	}
	if fv != 51.3012312 {
		t.Fatal()
	}

	fv1, e := c.GetFloat64("skaiciai", "pirmas")
	if e != nil {
		t.Fatal()
	}
	if fv1 != 51.3012312 {
		t.Fatal()
	}

	if !c.Exists("iplist", "10.10.10.10") {
		t.Fatal()
	}

	if !c.Exists("on connect", "hook3") {
		t.Fatal()
	}

	lv, e := c.GetKeysList("notifier")
	if e != nil {
		t.Fatal()
	}
	if len(lv) != 4 {
		t.Fatal()
	}

	lv1, e := c.GetKeys("notifier")
	if e != nil {
		t.Fatal()
	}
	if len(lv1) != 6 {
		t.Fatal()
	}

	c.Set("test", "test", "test")
	c.Write("config.test2.ini")
	c.Reset()
	c.Read("config.test2.ini")
	if c.GetStringDefault("test", "test", "test-10") != "test" {
		t.Fatal()
	}

	if len(c.Sections()) != 8 {
		t.Fatal()
	}

	c.Delete("test", "")

	if c.Exists("test", "test") {
		t.Fatal()
	}
}
