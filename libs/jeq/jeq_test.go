package jeq

import (
	"testing"
	"time"
)

func TestMain(t *testing.T) {
	j := New()

	var timeouts int
	var jobDone bool
	var response string

	j.OnTimeout = func(jobid string, jobpayload interface{}) {
		timeouts++
	}

	jobPayload := "job 1" // get from db

	jobId := j.Add(100*time.Millisecond, j.NewJob(jobPayload, func(i interface{}) {
		jobDone = true
		response = i.(string)
	}))

	time.Sleep(1 * time.Second)
	if timeouts < 6 {
		t.Fatal()
	}
	if jobDone {
		t.Fatal()
	}
	j.Emit(jobId, "response")
	if !jobDone {
		t.Fatal()
	}
	if response != "response" {
		t.Fatal()
	}
	j.Remove(jobId)
	tmp := timeouts
	time.Sleep(1 * time.Second)
	if tmp != timeouts {
		t.Fatal()
	}

}
