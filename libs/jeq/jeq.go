package jeq

import (
	"fmt"
	"sync"
	"time"
)

func New() *T {
	t := new(T)
	t.jobs = make(map[string]*Job)
	t.timers = make(map[string]*time.Timer)
	t.mu = &sync.Mutex{}
	return t
}

type Job struct {
	ID      string
	Payload interface{}
	cb      func(interface{})
}

type T struct {
	jobs   map[string]*Job
	timers map[string]*time.Timer
	mu     *sync.Mutex

	OnTimeout func(string, interface{})
}

func (t *T) NewJob(pl interface{}, cb func(interface{})) *Job {
	job := &Job{getIdString(), pl, cb}
	return job
}

func (t *T) Add(timeout time.Duration, job *Job) string {
	id := job.ID
	t.mu.Lock()
	t.jobs[id] = job
	t.timers[id] = time.AfterFunc(timeout, func() {
		id := id
		timeout := timeout
		job := job
		if t.OnTimeout != nil {
			t.OnTimeout(job.ID, job.Payload)
		}
		t.mu.Lock()
		t.timers[id].Reset(timeout)
		t.mu.Unlock()
	})
	t.mu.Unlock()
	return id
}

func (t *T) Remove(id string) {
	t.mu.Lock()
	_, ok := t.jobs[id]
	t.mu.Unlock()
	if ok {
		t.mu.Lock()
		t.timers[id].Stop()
		delete(t.timers, id)
		delete(t.jobs, id)
		t.mu.Unlock()
	}
}

func (t *T) Emit(id string, payload interface{}) {
	t.mu.Lock()
	job, ok := t.jobs[id]
	t.mu.Unlock()
	if ok {
		job.cb(payload)
	}
}

func getIdString() string {
	return fmt.Sprintf("%d", getId())
}

func getId() int64 {
	return time.Now().UnixNano()
}
