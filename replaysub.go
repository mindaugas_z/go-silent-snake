package ss

import (
	"bitbucket.org/mindaugas_z/go-silent-snake/libs/bolt-ses"
	"bitbucket.org/mindaugas_z/go-silent-snake/libs/deq"
	"bitbucket.org/mindaugas_z/go-silent-snake/libs/rfo"
	"bitbucket.org/mindaugas_z/go-silent-snake/libs/toe"
	"github.com/nats-io/go-nats"
	"github.com/nats-io/nuid"
	"time"
)

func (t *T) ReplayAndSubscribe(to int64, onMessage func(msg bes.LogMessage)) {

	from := time.Now().UnixNano()

	chName := nuid.New().Next()

	var m bes.LogMessage

	ch := make(chan struct{})

	que := deq.New()

	que.Subscribe(func(item deq.Item) {
		var m bes.LogMessage
		m.Decode(item.([]byte))
		onMessage(m)
	})

	var fmsg bes.LogMessage

	vtoe := toe.New(1*time.Second, func(t bool) {
		if !t {
			from = fmsg.ID
		}
	})

	cbfns := rfo.New(10)

	fmsg.ID = time.Now().UnixNano()

	// use global subscribtion
	cbfns.Add(func(b []byte) {
		fmsg.Decode(b)
		vtoe.Event()
	})

	// use global subscribtion
	t.pmq.Subscribe(t.namespace+"broadcast", func(msg *nats.Msg) {
		cbfns.Run(clone(msg.Data))
		que.WriteSecondary(clone(msg.Data))
	})

	vtoe.Wait()
	from = fmsg.ID

	timer := time.NewTimer(1 * time.Second)

	var ns *nats.Subscription
	ns, _ = t.pmq.Subscribe(t.namespace+"replay-"+chName, func(msg *nats.Msg) {
		m.Decode(msg.Data)
		timer.Reset(1 * time.Second)

		if from <= m.ID || m.ID == -1 {
			que.Switch()
			ns.Unsubscribe()
			close(ch)
			return
		}

		if m.ID != -1 {
			que.WritePrimary(msg.Data)
		}

		if from >= m.ID {
			t.request(chName, m)
		}

	})
	m.ID = to
	t.request(chName, m)

	go func() {
		timer.Reset(1 * time.Second)
		for {
			select {
			case <-timer.C:
				timer.Reset(1 * time.Second)
				t.request(chName, m)
			case <-ch:
				timer.Stop()
				return
			}
		}
	}()

}

func (t *T) ReplayAndSubscribeWithPrefix(prefix string, to int64, onMessage func(msg bes.LogMessage)) {

	from := time.Now().UnixNano()

	chName := nuid.New().Next()

	var m bes.LogMessage

	ch := make(chan struct{})

	que := deq.New()

	que.Subscribe(func(item deq.Item) {
		var m bes.LogMessage
		m.Decode(item.([]byte))
		onMessage(m)
	})

	var fmsg bes.LogMessage

	vtoe := toe.New(1*time.Second, func(t bool) {
		if !t {
			from = fmsg.ID
		}
	})

	cbfns := rfo.New(10)

	fmsg.ID = time.Now().UnixNano()

	// use global subscribtion
	cbfns.Add(func(b []byte) {
		fmsg.Decode(b)
		vtoe.Event()
	})

	// use global subscribtion
	t.pmq.Subscribe(t.namespace+"broadcast", func(msg *nats.Msg) {
		cbfns.Run(clone(msg.Data))

		var m bes.LogMessage
		m.Decode(msg.Data)
		if m.Prefix == prefix {
			que.WriteSecondary(clone(msg.Data))
		}
	})

	vtoe.Wait()
	from = fmsg.ID

	timer := time.NewTimer(1 * time.Second)

	var ns *nats.Subscription
	ns, _ = t.pmq.Subscribe(t.namespace+"replay-prefix-"+chName, func(msg *nats.Msg) {
		m.Decode(msg.Data)
		timer.Reset(1 * time.Second)

		if from <= m.ID || m.ID == -1 {
			que.Switch()
			ns.Unsubscribe()
			close(ch)
			return
		}

		if m.ID != -1 && m.Prefix == prefix {
			que.WritePrimary(msg.Data)
		}

		if from >= m.ID {
			t.request3(prefix, chName, m)
		}

	})
	m.ID = to
	t.request3(prefix, chName, m)

	go func() {
		timer.Reset(1 * time.Second)
		for {
			select {
			case <-timer.C:
				timer.Reset(1 * time.Second)
				t.request3(prefix, chName, m)
			case <-ch:
				timer.Stop()
				return
			}
		}
	}()

}
