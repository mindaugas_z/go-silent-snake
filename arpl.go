package ss

import (
	// "fmt"
	"github.com/nats-io/go-nats"
	"time"
)

func (t *T) SPublish(msg string) {
	t.pmq.Publish(t.namespace+"-election", []byte(msg))
}

func (t *T) prep20() {
	t.sfr.Bind("synced-servers-"+t.masterTopic, func(server string) {
		if server != t.masterTopic {
			if t.so.Set(server) {
				t.onFoundSynced()
			}
		}
	})

	t.sfr.Bind("master-"+t.masterTopic, func(master string) {
		if master != t.masterTopic {
			if !t.mr.Check(master) {
				t.mr.Register(master)
			}
			if t.isMaster {
				t.SPublish("master-repl-" + master + " " + t.masterTopic)
			}
		}
	})

	t.sfr.Bind("master-repl-"+t.masterTopic, func(server string) {
		if t.masterTopic != server {
			t.startSync <- server
		}
	})

	t.sfr.Bind("slave", func(server string) {

	})

	t.sfr.Bind("new-slave", func(slave string) {
		if slave != t.masterTopic {
			if t.isMaster {
				t.SPublish("master-" + slave + " " + t.masterTopic)
			}
		}
	})

	t.sfr.Bind("new-master", func(master string) {
		if master != t.masterTopic {
			if !t.mr.Check(master) {
				t.mr.Register(master)
			}
			if t.isMaster {
				if t.inSync {
					t.SPublish("synced-servers-" + master + " " + t.masterTopic)
				}
				time.Sleep(100 * time.Millisecond)

				t.SPublish("master-" + master + " " + t.masterTopic)
				t.SPublish("master-repl-" + master + " " + t.masterTopic)
			}
		}
	})

	t.pmq.Subscribe(t.namespace+"-election", func(msg *nats.Msg) {
		t.sfr.Run(string(msg.Data))
	})

	time.Sleep(100 * time.Millisecond)
	if t.isMaster {
		t.SPublish("new-master " + t.masterTopic)
	} else {
		t.SPublish("new-slave " + t.masterTopic)
	}
}

// func (t *T) Publish()
