package ss

import (
	"bitbucket.org/mindaugas_z/go-silent-snake/libs/bolt-ses"
	"github.com/nats-io/go-nats"
	"github.com/nats-io/nuid"
	"time"
)

func (t *T) Replay(from, to int64, onMessage func(msg bes.LogMessage), onDone func()) {
	chName := t.namespace + nuid.New().Next()

	start := from
	if to < from {
		start = to
	}
	end := to
	if from > end {
		end = from
	}

	var m bes.LogMessage

	ch := make(chan struct{})

	timer := time.NewTimer(1 * time.Second)

	var ns *nats.Subscription
	ns, _ = t.pmq.Subscribe(t.namespace+"replay-"+chName, func(msg *nats.Msg) {
		m.Decode(msg.Data)

		timer.Reset(1 * time.Second)

		if m.ID != -1 {
			onMessage(m)
		}

		if end <= m.ID || m.ID == -1 {
			ns.Unsubscribe()
			close(ch)
			go func() {
				onDone()
			}()
			return
		}

		if end >= m.ID {
			t.request(chName, m)
		}
	})
	m.ID = start
	t.request(chName, m)

	go func() {
		for {
			select {
			case <-timer.C:
				timer.Reset(1 * time.Second)
				t.mr.Next()
				t.request(chName, m)
			case <-ch:
				timer.Stop()
				return
			}
		}
	}()
}

func (t *T) ReplayWithPrefix(prefix string, from int64, to int64, onMessage func(msg bes.LogMessage), onDone func()) {
	chName := t.namespace + nuid.New().Next()

	start := from
	if to < from {
		start = to
	}
	end := to
	if from > end {
		end = from
	}

	var m bes.LogMessage

	ch := make(chan struct{})

	timer := time.NewTimer(1 * time.Second)

	var ns *nats.Subscription
	ns, _ = t.pmq.Subscribe(t.namespace+"replay-prefix-"+chName, func(msg *nats.Msg) {
		m.Decode(msg.Data)

		timer.Reset(1 * time.Second)

		if m.ID != -1 {
			onMessage(m)
		}

		if end <= m.ID || m.ID == -1 {
			ns.Unsubscribe()
			close(ch)
			go func() {
				onDone()
			}()
			return
		}

		if end >= m.ID {
			t.request3(prefix, chName, m)
		}
	})
	m.ID = start
	t.request3(prefix, chName, m)

	go func() {
		for {
			select {
			case <-timer.C:
				timer.Reset(1 * time.Second)
				t.mr.Next()
				t.request3(prefix, chName, m)
			case <-ch:
				timer.Stop()
				return
			}
		}
	}()
}
