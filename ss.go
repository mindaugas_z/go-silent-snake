package ss

import (
	"encoding/binary"
	"encoding/json"
	"fmt"
	"github.com/boltdb/bolt"
	"os"
	"sync"
	// gnatsd "github.com/nats-io/gnatsd/server"
	"bitbucket.org/mindaugas_z/go-silent-snake/libs/bolt-ses"
	"bitbucket.org/mindaugas_z/go-silent-snake/libs/downtime-log"
	"bitbucket.org/mindaugas_z/go-silent-snake/libs/fnobserver"
	"bitbucket.org/mindaugas_z/go-silent-snake/libs/lp-so"
	"bitbucket.org/mindaugas_z/go-silent-snake/libs/rfo"
	"bitbucket.org/mindaugas_z/go-silent-snake/libs/sfr"
	"github.com/nats-io/go-nats"
	"github.com/nats-io/nuid"
	"strings"
	"sync/atomic"
	"time"
)

func (t *T) onSyncDone() {
	if t.appStarted {
		return
	}
	t.appStarted = true
	if t.SafeStartApp != nil {
		t.SafeStartApp()
	}
}

func (t *T) onFoundSynced() {
	if t.appStarted {
		return
	}
	t.appStarted = true
	if t.SafeStartApp != nil {
		t.SafeStartApp()
	}
}

func (t *T) startApp() {
	if t.appStarted {
		return
	}
	t.appStarted = true
	if t.SafeStartApp != nil {
		t.SafeStartApp()
	}
}

func (t *T) SingleNode() {
	t.singleApp = true
}

func New(db *bolt.DB, natsServers string, namespace string) *T {
	t := new(T)

	t.namespace = namespace + "-"

	t.db = db

	t.onMessageOnce = rfo.New(100)
	t.sfr = sfr.New(1000)
	t.so = lpso.New()

	t.startSync = make(chan string)

	t.prepare(natsServers)

	t.cbs = fnobserver.New()

	t.mr = NewMR(db, t.namespace, "0")

	return t
}

type T struct {
	namespace     string
	onMessageOnce *rfo.T

	so         *lpso.SetOnce
	dl         *DowntimeLog.DowntimeLog
	eventStore *bes.T

	mr *MR

	cbs *fnobserver.T

	appStarted bool
	singleApp  bool

	sfr *sfr.T

	shardID  int64
	isMaster bool

	db *bolt.DB

	pmq *nats.Conn

	masterTopic string

	inSync    bool
	startSync chan string

	SafeStartApp func()

	OnLogMessage func(bes.LogMessage)

	OnNatsGone func()
}

// Delete older then d. eg: time.Now.UnixNano() - 24*time.Hour.Nanoseconds()
func (t *T) Delete(d int64) {
	t.eventStore.Delete(d)
}

func (t *T) prepare(natsServers string) {
	var err error

	t.pmq, err = nats.Connect(natsServers, nats.DisconnectHandler(func(nc *nats.Conn) {
		if t.OnNatsGone != nil {
			t.OnNatsGone()
		} else {
			panic("Disconnected")
		}
	}))

	if err != nil {
		panic(err.Error())
	}
}

func (t *T) SetMaster() {
	t.isMaster = true
}

func (t *T) ShardID(id int64) {
	t.shardID = id
}

func (t *T) OnStreamMessage(fn func(bes.LogMessage)) {
	t.cbs.Add(fn)
}

func (t *T) InitAndExit() {
	t.init(true)
	os.Exit(0)
}

func (t *T) StartApp() {
	t.startApp()
}

func (t *T) init(force bool) {
	t.db.Update(func(tx *bolt.Tx) error {
		tx.CreateBucketIfNotExists([]byte(string(t.namespace) + "m"))
		tx.CreateBucketIfNotExists([]byte(string(t.namespace) + "history"))
		tx.CreateBucketIfNotExists([]byte(string(t.namespace) + "masters"))
		return nil
	})

	t.db.Update(func(tx *bolt.Tx) error {
		bb := tx.Bucket([]byte(string(t.namespace) + "m"))
		mt := bb.Get([]byte("masterTopic"))
		if mt == nil || force {
			t.masterTopic = nuid.New().Next()
			bb.Put([]byte("masterTopic"), []byte(t.masterTopic))
		} else {
			t.masterTopic = string(mt)
		}
		return nil
	})
}

func (t *T) Start() {

	// load libs
	t.dl = DowntimeLog.New(t.db, t.namespace+"downtime-log")
	t.eventStore = bes.New(t.db, t.namespace+"binlog2", t.shardID)

	t.init(false)

	t.mr.SetDefault(t.masterTopic)

	// subscribe event stream
	t.pmq.Subscribe(t.namespace+"broadcast", t.binlogWritter) // write to db

	// downtime log
	dlLastSeen := t.dl.LastSeen()
	t.dl.StartHeartbeat(1 * time.Second)

	if dlLastSeen == -1 {
		if dlLast := t.dl.Last(); dlLast != nil {
			dlLastSeen = dlLast.To
		}
	}
	if dlLastSeen != -1 {
		t.dl.Register(dlLastSeen, time.Now().UnixNano())
	}

	// println("me: " + t.masterTopic)

	var ml sync.Mutex
	var cond = sync.NewCond(&ml)

	var needCounter int64
	var doneCounter int64

	var smast = NewMR(t.db, t.namespace, "1")

	// we need replay all missing intervals from all known masters
	if t.isMaster {
		go func() {
			for {
				// wait master to register
				master := <-t.startSync

				// check if we already "replicated" new master
				if smast.Check(master) {
					continue
				}
				smast.Register(master)
				_ = master

				go func() {

					// known masters count * downtimes
					needCounter = int64(t.dl.Count() * (t.mr.KnownMastersCount() - 1))

					// iterate over downtimes and replay missing intervals
					t.dl.Run(func(dli DowntimeLog.DowntimeLogItem) {
						master := master

						// t.playAny(dli.To, dli.From, func(m bes.LogMessage) {
						// 	println(m.Data.(string))
						// }, func() { println("done") })

						t.playLog(master, dli.To, dli.From, func() {
							atomic.AddInt64(&doneCounter, 1)
							cond.L.Lock()
							cond.Broadcast()
							cond.L.Unlock()
						}, func() {
							atomic.AddInt64(&doneCounter, 1)
							cond.L.Lock()
							cond.Broadcast()
							cond.L.Unlock()
							// fmt.Printf("no messages found on %s for (from %d to %d)\n", master, dli.From, dli.To)
						})
					})
				}()
			}
		}()
	}

	if t.isMaster {
		go func() {
			for {
				cond.L.Lock()
				cond.Wait()
				cond.L.Unlock()
				// fmt.Printf("%v %v\n", needCounter, atomic.LoadInt64(&doneCounter))
				if needCounter <= atomic.LoadInt64(&doneCounter) {
					t.dl.Flush()
					t.inSync = true
					t.onSyncDone()
				}
			}
		}()
	} else {
		t.dl.Flush()
		t.onSyncDone()
	}

	t.prep20()

	// use commands channel
	t.pmq.Subscribe(t.namespace+"control", func(msg *nats.Msg) {
		m := strings.SplitN(string(msg.Data), " ", 2)
		if m[0] == "set-insync" {
			t.dl.Flush()
			t.inSync = true
		}
		if m[0] == "reset-masters" {
			// create list of masters of now
			t.mr.ClearKnownMasters()
			t.dl.Flush()
			t.inSync = true
			t.pmq.Publish(t.namespace+"control", []byte("master "+t.masterTopic))
			t.onSyncDone()
		}
		if m[0] == "master" {
			t.mr.Register(m[1])
		}
	})

	// play next message from get message
	t.pmq.Subscribe(t.namespace+"replay-request-"+t.masterTopic, func(msg *nats.Msg) {
		args := strings.SplitN(string(msg.Data), " ", 2)

		var m bes.LogMessage
		json.Unmarshal([]byte(args[1]), &m)

		repl := fmt.Sprintf("replay-%s", args[0])

		var ms bes.LogMessage
		ms = t.eventStore.Next(m.ID)

		t.pmq.Publish(t.namespace+repl, ms.Encode())
	})

	t.pmq.Subscribe(t.namespace+"replay-prefix-request-"+t.masterTopic, func(msg *nats.Msg) {
		args := strings.SplitN(string(msg.Data), " ", 3)

		var m bes.LogMessage
		json.Unmarshal([]byte(args[1]), &m)

		repl := fmt.Sprintf("replay-prefix-%s", args[0])

		var ms bes.LogMessage
		ms = t.eventStore.NextWithPrefix(args[2], m.ID)

		t.pmq.Publish(t.namespace+repl, ms.Encode())
	})

	if t.singleApp {
		t.startApp()
	}

}

func (t *T) PublishToStore(msg interface{}) int64 {
	id := time.Now().UnixNano()
	id = id - (id % 100) + t.shardID
	m := bes.LogMessage{ID: id, Prefix: "!__noprefix__!", Data: msg}
	t.pmq.Publish(t.namespace+"broadcast", m.Encode())
	return id
}

func (t *T) PublishToStoreWithprefix(prefix string, msg interface{}) int64 {
	id := time.Now().UnixNano()
	id = id - (id % 100) + t.shardID
	m := bes.LogMessage{ID: id, Prefix: prefix, Data: msg}
	t.pmq.Publish(t.namespace+"broadcast", m.Encode())
	return id
}

func (t *T) request(topic string, msg bes.LogMessage) {
	server := t.mr.Current()
	if t.so.Check() {
		server = t.so.Get().(string)
	}
	ms := fmt.Sprintf("%s %s", topic, msg.Encode())
	t.pmq.Publish(t.namespace+"replay-request-"+server, []byte(ms))
}

func (t *T) request2(topic, master string, msg bes.LogMessage) {
	ms := fmt.Sprintf("%s %s", topic, msg.Encode())
	t.pmq.Publish(t.namespace+"replay-request-"+master, []byte(ms))
}

func (t *T) request3(prefix, topic string, msg bes.LogMessage) {
	server := t.mr.Current()
	if t.so.Check() {
		server = t.so.Get().(string)
	}
	ms := fmt.Sprintf("%s %s %s", topic, msg.Encode(), prefix)
	t.pmq.Publish(t.namespace+"replay-prefix-request-"+server, []byte(ms))
}

func (t *T) binlogWritter(msg *nats.Msg) {

	var m bes.LogMessage
	m.Decode(msg.Data)

	if t.isMaster {
		t.eventStore.WriteRaw(m)
		t.eventStore.Notify()
	}

	t.onMessageOnce.Run(clone(msg.Data))

	t.cbs.Run(m)
	if t.OnLogMessage != nil {
		t.OnLogMessage(m)
	}
}

func itob(v int64) []byte {
	b := make([]byte, 8)
	binary.BigEndian.PutUint64(b, uint64(v))
	return b
}

func chClosed(ch chan struct{}) bool {
	select {
	case <-ch:
		return true
	default:
	}
	return false
}

func clone(b []byte) []byte {
	r := make([]byte, len(b))
	copy(r, b)
	return r
}
