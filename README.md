# Silent Snake

UNSAFE distributed streaming event log

Proof-of-Concept

- "libs/bolt-ses" lib for basic streaming event store 
- "libs/htc" http streaming cache (unsafe)


// atention: api not finished, project unfinished, very unstable
// dont use in production 


feel free to fork, edit, report issues

current coverage: >60.00000%

```go
id = GetUnixNano()
id = id - (id % 100 ) + shardN[0..99]

id = PublishToStore(blob)
Replay(fromUnixNano, toUnixNano, func(msg []byte) {})
ReplayAndSubscribe(fromUnixNano, func(msg []byte) {})
ReplayWithPrefix(prefix, fromUnixNano, toUnixNano, func(msg []byte) {})
ReplayAndSubscribeWithPrefix(prefix, fromUnixNano, func(msg []byte) {})
```


sample
```go

// channel for non-block
// var blockCh chan struct{} = make(chan struct{})
func main() {
	// close channel to no block
	// close(blockCh)

	ch := make(chan struct{})


	db, err := bolt.Open("db.db", 0755, nil)
	if err != nil {
		panic(err.Error())
	}

	s = ss.New(db, "nats://127.0.0.1:4222", "my-namespace")

    s.SetMaster()

    // 
	s.ShardID(*shardID)

    // if not set - default panic
    s.OnNatsGone = func() {
		// panic("") 
		close(ch)

		// create chanel to block operation
		//blockCh = make(chan struct{})
	}

    // calls on binlog sync done or on found in sync master
	s.SafeStartApp = func() {
		go func() {
			for {
				select {
				case _, ok := <-ch:
					if !ok {
						// gracefull shutdown
					}
				default:
				}
				// or block operation if channel not closed
				// <-blockCh
				s.PublishToStore(fmt.Sprintf("message: %d", time.Now().Unix()))
				time.Sleep(time.Second)
			}
		}()

		from := time.Now().UnixNano() - int64(90*time.Second)
		s.ReplayAndSubscribe(from, func(v bes.LogMessage) {
			fmt.Printf("play %s\n", v.Encode())
		})
	}
	s.OnLogMessage = func(msg bes.LogMessage) {
		// println(msg.Data.(string))
	}

	s.Start()

	runtime.Goexit()
}
```

pseudo code:
```
pointer = items.add(struct{id,blob})
indexN.add(pointer)
...
index.replay(from,to)
```
