package ss

import (
	"bitbucket.org/mindaugas_z/go-silent-snake/libs/bolt-ses"
	"github.com/boltdb/bolt"
	"github.com/nats-io/go-nats"
	"os"
	"sync/atomic"
	"testing"
	"time"
)

const ns string = `nats://127.0.0.1:4222`

func cmd(a, b string) string {
	return a + " " + b
}

func TestRepl(t *testing.T) {
	defer os.Remove("testing1.db")
	defer os.Remove("testing2.db")
	defer os.Remove("testing3.db")

	db1, _ := bolt.Open("testing1.db", 0755, nil)
	db2, _ := bolt.Open("testing2.db", 0755, nil)
	db3, _ := bolt.Open("testing3.db", 0755, nil)

	s1 := New(db1, ns, "testing1")
	s2 := New(db2, ns, "testing1")
	s3 := New(db3, ns, "testing1")

	cmds := []string{}

	s1.pmq.Subscribe(s1.namespace+"-election", func(msg *nats.Msg) {
		cmds = append(cmds, string(msg.Data))
	})

	s1.SetMaster()
	s1.Start()
	s1.OnNatsGone = func() {}
	s1.inSync = true

	s2.SetMaster()
	s2.Start()
	s2.OnNatsGone = func() {}

	s3.Start()
	s3.OnNatsGone = func() {}

	time.Sleep(1000 * time.Millisecond)

	if !s2.inSync {
		t.Fatal()
	}

	if !s2.mr.Check(s1.masterTopic) {
		t.Fatal()
	}
	if !s1.mr.Check(s2.masterTopic) {
		t.Fatal()
	}
}

func TestReplDowntime(t *testing.T) {
	defer os.Remove("testing100.db")
	defer os.Remove("testing200.db")

	db1, _ := bolt.Open("testing100.db", 0755, nil)
	db2, _ := bolt.Open("testing200.db", 0755, nil)

	s1 := New(db1, ns, "testing2")
	s2 := New(db2, ns, "testing2")

	cmds := []string{}

	s1.pmq.Subscribe(s1.namespace+"-election", func(msg *nats.Msg) {
		cmds = append(cmds, string(msg.Data))
	})

	s1.SetMaster()
	s1.Start()
	s1.OnNatsGone = func() {}
	s1.inSync = true

	s2.SetMaster()
	s2.Start()
	s2.OnNatsGone = func() {}

	time.Sleep(100 * time.Millisecond)
	s2.dl.Stop()
	s2.pmq.Close()
	time.Sleep(500 * time.Millisecond)

	id1 := s1.PublishToStore("1st msg")
	id2 := s1.PublishToStore("2st msg")

	time.Sleep(200 * time.Millisecond)

	s2 = New(db2, ns, "testing2")
	s2.OnNatsGone = func() {}
	s2.SetMaster()
	s2.prepare(ns)
	s2.Start()

	time.Sleep(1000 * time.Millisecond)

	if !s2.inSync {
		t.Fatal()
	}

	if !s1.eventStore.Check(id1) {
		t.Fatal()
	}
	if !s1.eventStore.Check(id2) {
		t.Fatal()
	}

	if !s2.eventStore.Check(id1) {
		t.Fatal()
	}
	if !s2.eventStore.Check(id2) {
		t.Fatal()
	}

	if !s2.inSync {
		t.Fatal()
	}

	if !s2.mr.Check(s1.masterTopic) {
		t.Fatal()
	}
	if !s1.mr.Check(s2.masterTopic) {
		t.Fatal()
	}

	var c int
	s2.Replay(id1, id2, func(msg bes.LogMessage) {
		c++
	}, func() {})
	time.Sleep(100 * time.Millisecond)

	if c != 1 {
		t.Fatal()
	}
	c = 0
	s2.Replay(id1-1, id2, func(msg bes.LogMessage) {
		c++
	}, func() {})
	time.Sleep(100 * time.Millisecond)

	if c != 2 {
		t.Fatal()
	}
}

func TestRepl2(t *testing.T) {
	defer os.Remove("testing1.db")
	defer os.Remove("testing2.db")

	db1, _ := bolt.Open("testing1.db", 0755, nil)
	db2, _ := bolt.Open("testing2.db", 0755, nil)

	s1 := New(db1, ns, "testing3")
	s2 := New(db2, ns, "testing3")

	var sas1 bool
	var sas2 bool

	s1.SetMaster()
	s1.Start()
	s1.OnNatsGone = func() {}
	s1.SafeStartApp = func() {
		sas1 = true
	}

	s2.SetMaster()
	s2.Start()

	s2.OnNatsGone = func() {}
	s2.SafeStartApp = func() {
		sas2 = true
	}

	time.Sleep(100 * time.Millisecond)

	id1 := s1.PublishToStore("1st msg")
	s2.PublishToStore("2st msg")

	time.Sleep(100 * time.Millisecond)

	var c1 int
	s2.ReplayAndSubscribe(id1-1, func(msg bes.LogMessage) {
		c1++
	})
	time.Sleep(100 * time.Millisecond)

	s2.PublishToStore("3rd msg")

	time.Sleep(100 * time.Millisecond)

	if c1 != 3 {
		t.Fatal(c1)
	}

	if !sas1 {
		t.Fatal()
	}

	if !sas2 {
		t.Fatal()
	}
}

func TestJoinStream(t *testing.T) {
	defer os.Remove("testing10.db")
	defer os.Remove("testing20.db")

	db1, _ := bolt.Open("testing10.db", 0755, nil)
	db2, _ := bolt.Open("testing20.db", 0755, nil)

	s1 := New(db1, ns, "testing4")
	s2 := New(db2, ns, "testing4")

	var sas1 bool
	var sas2 bool

	s1.SetMaster()
	s1.Start()
	s1.OnNatsGone = func() {}
	s1.SafeStartApp = func() {
		sas1 = true
	}

	s2.SetMaster()
	s2.Start()
	s2.OnNatsGone = func() {}
	s2.SafeStartApp = func() {
		sas2 = true
	}

	time.Sleep(1000 * time.Millisecond)

	if !sas1 {
		t.Fatal()
	}

	if !sas2 {
		t.Fatal()
	}

	start := time.Now().UnixNano()
	n := uint64(0)
	go func() {
		for i := 0; i < 50000; i++ {
			atomic.StoreUint64(&n, uint64(i))

			s1.PublishToStore(i)
			time.Sleep(10 * time.Microsecond)
		}
	}()

	time.Sleep(20 * time.Millisecond)
	var nums []int

	s2.ReplayAndSubscribe(start, func(msg bes.LogMessage) {
		nums = append(nums, int(msg.Data.(float64)))
	})

	time.Sleep(3 * time.Second)

	if len(nums) < 2 {
		t.Fatal()
	}

	if nums[0] != 0 {
		t.Fatal(nums[0])
	}

	for i := 0; i < len(nums)-2; i++ {
		if (nums[i] + 1) != nums[i+1] {
			t.Fatal(nums[i], nums[i+1])
		}
	}
}

func TestJoinStream1(t *testing.T) {
	defer os.Remove("testing11.db")
	defer os.Remove("testing21.db")

	db1, _ := bolt.Open("testing11.db", 0755, nil)
	db2, _ := bolt.Open("testing21.db", 0755, nil)

	s1 := New(db1, ns, "testing5")
	s2 := New(db2, ns, "testing5")

	var sas1 bool
	var sas2 bool

	s1.SetMaster()
	s1.Start()
	s1.OnNatsGone = func() {}
	s1.SafeStartApp = func() {
		sas1 = true
	}

	s2.SetMaster()
	s2.Start()
	s2.OnNatsGone = func() {}
	s2.SafeStartApp = func() {
		sas2 = true
	}

	time.Sleep(1000 * time.Millisecond)

	if !sas1 {
		t.Fatal()
	}

	if !sas2 {
		t.Fatal()
	}

	n := uint64(0)
	go func() {
		for i := 0; i < 50000; i++ {
			atomic.StoreUint64(&n, uint64(i))
			s1.PublishToStore(i)
			time.Sleep(10 * time.Microsecond)
		}
	}()
	time.Sleep(10 * time.Millisecond)

	var nums []int

	s2.ReplayAndSubscribe(0, func(msg bes.LogMessage) {
		nums = append(nums, int(msg.Data.(float64)))
	})

	time.Sleep(3 * time.Second)

	if len(nums) < 2 {
		t.Fatal()
	}

	if nums[0] != 0 {
		t.Fatal(nums[0])
	}

	for i := 0; i < len(nums)-2; i++ {
		if (nums[i] + 1) != nums[i+1] {
			t.Fatal(nums[i], nums[i+1])
		}
	}
}

func TestJoinStreamWithPrefix(t *testing.T) {
	defer os.Remove("testing111.db")
	defer os.Remove("testing211.db")

	db1, _ := bolt.Open("testing111.db", 0755, nil)
	db2, _ := bolt.Open("testing211.db", 0755, nil)

	s1 := New(db1, ns, "testing6")
	s2 := New(db2, ns, "testing6")

	var sas1 bool
	var sas2 bool

	s1.SetMaster()
	s1.Start()
	s1.OnNatsGone = func() {}
	s1.SafeStartApp = func() {
		sas1 = true
	}

	s2.SetMaster()
	s2.Start()
	s2.OnNatsGone = func() {}
	s2.SafeStartApp = func() {
		sas2 = true
	}

	time.Sleep(1000 * time.Millisecond)

	if !sas1 {
		t.Fatal()
	}

	if !sas2 {
		t.Fatal()
	}

	n := uint64(0)
	go func() {
		for i := 0; i < 50000; i++ {
			atomic.StoreUint64(&n, uint64(i))
			s1.PublishToStoreWithprefix("1st", i)
			time.Sleep(10 * time.Microsecond)
		}
	}()
	time.Sleep(10 * time.Millisecond)

	var nums []int

	s2.ReplayAndSubscribeWithPrefix("1st", 0, func(msg bes.LogMessage) {
		nums = append(nums, int(msg.Data.(float64)))
	})

	time.Sleep(3 * time.Second)

	if len(nums) < 2 {
		t.Fatal()
	}

	if nums[0] != 0 {
		t.Fatal(nums[0])
	}

	for i := 0; i < len(nums)-2; i++ {
		if (nums[i] + 1) != nums[i+1] {
			t.Fatal(nums[i], nums[i+1])
		}
	}
}
