package ss

import (
	"bitbucket.org/mindaugas_z/go-silent-snake/libs/bolt-ses"
	"github.com/nats-io/go-nats"
	"github.com/nats-io/nuid"
	"time"
)

//TODO: remove this method
func (t *T) playLog(master string, from, to int64, onDone func(), onFail func()) {
	var m bes.LogMessage

	start := from
	if to < from {
		start = to
	}
	end := to
	if from > end {
		end = from
	}

	chName := nuid.New().Next()

	ch := make(chan struct{})

	timer := time.NewTimer(1 * time.Second)

	var ns *nats.Subscription

	ns, _ = t.pmq.Subscribe(t.namespace+"replay-"+chName, func(msg *nats.Msg) {
		m.Decode(msg.Data)

		timer.Reset(1 * time.Second)

		if end < m.ID {
			close(ch)
			go onDone()
			ns.Unsubscribe()
			return
		}

		if m.ID != -1 {
			if !t.eventStore.Check(m.ID) {
				t.eventStore.WriteRaw(m)
				// maybe call something
			}
		} else {
			close(ch)
			go onDone()
			ns.Unsubscribe()
			return
		}

		if end > m.ID && m.ID != -1 {
			t.request2(chName, master, m)
		}
	})

	m.ID = start
	t.request2(chName, master, m)
	timer.Reset(1 * time.Second)
	go func() {
		for {
			select {
			case <-timer.C:
				t.request2(chName, master, m)
				timer.Reset(1 * time.Second)
			case <-ch:
				timer.Stop()
				return
			}
		}
	}()
}
